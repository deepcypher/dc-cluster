# Base

This directory is used to define the state of the base cluster, such as standard services, and deployments expected in all clusters. This is also what ensures infrastructure is being called (where it is applicable to all clusters).
