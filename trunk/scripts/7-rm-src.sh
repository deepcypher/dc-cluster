#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-05-02T12:51:19+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-07T12:46:40+01:00
# @License: please see LICENSE file in project root

# delete the repository source which held the kustomization
flux delete source git dc

# delete the kustomization itself
flux delete kustomization dc
