#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-05-02T12:51:19+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-03T00:33:11+01:00
# @License: please see LICENSE file in project root

# delete the monitoring kustomization source
flux delete kustomization monitoring

# delete the monitoring repository source which held the kustomization
flux delete source git monitoring
