#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-05-02T12:51:19+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-02T12:57:03+01:00
# @License: please see LICENSE file in project root

# uninstall flux and everything it held
flux uninstall
