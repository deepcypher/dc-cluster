#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-05-02T12:51:19+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-08T21:01:19+01:00
# @License: please see LICENSE file in project root


# link flux to our repository
flux create source git dc --url "https://gitlab.com/deepcypher/dc-cluster" --interval 1m --branch $(git branch --show-current) || exit 1

# tell flux where to look in this repository and start syncing (prune allows to delete unused resources if changes happen)
flux create kustomization dc --source GitRepository/dc --path "./cluster/overlays/bloated" --prune true --interval 1m

# display all flux components to user
flux get all
