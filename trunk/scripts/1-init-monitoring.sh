#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-05-02T12:51:19+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-03T00:39:50+01:00
# @License: please see LICENSE file in project root

flux create source git monitoring \
  --interval=30m \
  --url=https://github.com/fluxcd/flux2 \
  --branch=main


flux create kustomization monitoring \
  --interval=1h \
  --prune=true \
  --source=monitoring \
  --path="./manifests/monitoring" \
  --health-check="Deployment/prometheus.flux-system" \
  --health-check="Deployment/grafana.flux-system"

# NOTE:

# to access the monitoring without ingress:
# kubectl -n flux-system port-forward svc/grafana 3000:3000

# the control plane dashboard:
# http://localhost:3000/d/gitops-toolkit-control-plane

# The cluster reconciliation dashboard:
# http://localhost:3000/d/gitops-toolkit-cluster
