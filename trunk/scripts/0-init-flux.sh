#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-05-02T12:51:19+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-02T12:51:43+01:00
# @License: please see LICENSE file in project root

# this assumed you have a cluster up and running, kubectl config configured
# and that flux is installed E.G pikaur -S flux-go
flux check --pre || exit 1

# this is just to prevent a bug with flux thinking its still shutting down in cases where this happened recentley but was fully terminated
kubectl create namespace flux-system

# install flux components to cluster
flux install || exit 1
