#!/usr/bin/env bash

# @Author: George Onoufriou <archer>
# @Date:   2021-07-15T16:25:41+01:00
# @Last modified by:   archer
# @Last modified time: 2021-07-16T13:04:47+01:00

# find our IP address (assuming this is control node with stacked etcd)
ipad=$(ip addr show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)

echo "Probing members from ${ipad}:2379 :"
# get the status of etcd
ETCDCTL_API=3 etcdctl --endpoints "${ipad}:2379" \
                      --cert=/etc/kubernetes/pki/etcd/server.crt \
                      --key=/etc/kubernetes/pki/etcd/server.key \
                      --cacert=/etc/kubernetes/pki/etcd/ca.crt \
                      --write-out=table \
                      member list

echo "Probing endpoint status from ${ipad}:2379 :"
# get the status of etcd
ETCDCTL_API=3 etcdctl --endpoints "${ipad}:2379" \
                      --cert=/etc/kubernetes/pki/etcd/server.crt \
                      --key=/etc/kubernetes/pki/etcd/server.key \
                      --cacert=/etc/kubernetes/pki/etcd/ca.crt \
                      --write-out=table \
                      endpoint status
