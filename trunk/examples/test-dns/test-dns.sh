#!/usr/bin/env bash

# @Author: George Onoufriou <archer>
# @Date:   2021-07-15T16:25:41+01:00
# @Last modified by:   archer
# @Last modified time: 2021-07-21T14:42:56+01:00


echo "Launching dnsutils pod:"
kubectl apply -f https://k8s.io/examples/admin/dns/dnsutils.yaml
sleep 15

echo "Get state of pod:"
kubectl get pods dnsutils
sleep 5

echo "nslookup for kubernetes.default:"
kubectl exec -i -t dnsutils -- nslookup kubernetes.default
sleep 5

echo "nslookup for google.com:"
kubectl exec -i -t dnsutils -- nslookup google.com
sleep 5

echo "What is in our /etc/resolv.conf file:"
kubectl exec -ti dnsutils -- cat /etc/resolv.conf
sleep 5

echo "interactive:"
kubectl exec -i -t dnsutils -- /bin/sh
sleep 5

echo "Destroying pod:"
kubectl delete -f https://k8s.io/examples/admin/dns/dnsutils.yaml
sleep 5
